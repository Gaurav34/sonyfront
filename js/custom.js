jQuery(function($) {

	'use strict';
	
	$(".loader").delay(1000).fadeOut("slow");
  $("#overlayer").delay(1000).fadeOut("slow");	

	var siteMenuClone = function() {

		$('.js-clone-nav').each(function() {
			var $this = $(this);
			$this.clone().attr('class', 'site-nav-wrap').appendTo('.site-mobile-menu-body');
		});


		setTimeout(function() {
			
			var counter = 0;
      $('.site-mobile-menu .has-children').each(function(){
        var $this = $(this);
        
        $this.prepend('<span class="arrow-collapse collapsed">');

        $this.find('.arrow-collapse').attr({
          'data-toggle' : 'collapse',
          'data-target' : '#collapseItem' + counter,
        });

        $this.find('> ul').attr({
          'class' : 'collapse',
          'id' : 'collapseItem' + counter,
        });

        counter++;

      });

    }, 1000);

		$('body').on('click', '.arrow-collapse', function(e) {
      var $this = $(this);
      if ( $this.closest('li').find('.collapse').hasClass('show') ) {
        $this.removeClass('active');
      } else {
        $this.addClass('active');
      }
      e.preventDefault();  
      
    });

		$(window).resize(function() {
			var $this = $(this),
				w = $this.width();

			if ( w > 768 ) {
				if ( $('body').hasClass('offcanvas-menu') ) {
					$('body').removeClass('offcanvas-menu');
				}
			}
		})

		$('body').on('click', '.js-menu-toggle', function(e) {
			var $this = $(this);
			e.preventDefault();

			if ( $('body').hasClass('offcanvas-menu') ) {
				$('body').removeClass('offcanvas-menu');
				$this.removeClass('active');
			} else {
				$('body').addClass('offcanvas-menu');
				$this.addClass('active');
			}
		}) 

		// click outisde offcanvas
		$(document).mouseup(function(e) {
	    var container = $(".site-mobile-menu");
	    if (!container.is(e.target) && container.has(e.target).length === 0) {
	      if ( $('body').hasClass('offcanvas-menu') ) {
					$('body').removeClass('offcanvas-menu');
				}
	    }
		});
	}; 
	siteMenuClone();


	var sitePlusMinus = function() {
		$('.js-btn-minus').on('click', function(e){
			e.preventDefault();
			if ( $(this).closest('.input-group').find('.form-control').val() != 0  ) {
				$(this).closest('.input-group').find('.form-control').val(parseInt($(this).closest('.input-group').find('.form-control').val()) - 1);
			} else {
				$(this).closest('.input-group').find('.form-control').val(parseInt(0));
			}
		});
		$('.js-btn-plus').on('click', function(e){
			e.preventDefault();
			$(this).closest('.input-group').find('.form-control').val(parseInt($(this).closest('.input-group').find('.form-control').val()) + 1);
		});
	};
	// sitePlusMinus();

   var siteIstotope = function() {
  	/* activate jquery isotope */
	  var $container = $('#posts').isotope({
	    itemSelector : '.item',
	    isFitWidth: true
	  });

	  $(window).resize(function(){
	    $container.isotope({
	      columnWidth: '.col-sm-3'
	    });
	  });
	  
	  $container.isotope({ filter: '*' });

	    // filter items on button click
	  $('#filters').on( 'click', 'button', function(e) {
	  	e.preventDefault();
	    var filterValue = $(this).attr('data-filter');
	    $container.isotope({ filter: filterValue });
	    $('#filters button').removeClass('active');
	    $(this).addClass('active');
	  });
  }

  siteIstotope();

  var fancyBoxInit = function() {
	  $('.fancybox').on('click', function() {
		  var visibleLinks = $('.fancybox');

		  $.fancybox.open( visibleLinks, {}, visibleLinks.index( this ) );

		  return false;
		});
	}
	fancyBoxInit();


	var stickyFillInit = function() {
		$(window).on('resize orientationchange', function() {
	    recalc();
	  }).resize();

	  function recalc() {
	  	if ( $('.jm-sticky-top').length > 0 ) {
		    var elements = $('.jm-sticky-top');
		    Stickyfill.add(elements);
	    }
	  }
	}
	stickyFillInit();


	// navigation
  var OnePageNavigation = function() {
    var navToggler = $('.site-menu-toggle');
   	$("body").on("click", ".main-menu li a[href^='#'], .smoothscroll[href^='#'], .site-mobile-menu .site-nav-wrap li a", function(e) {
      e.preventDefault();

      var hash = this.hash;

      $('html, body').animate({
        'scrollTop': $(hash).offset().top
      }, 600, 'easeInOutCirc', function(){
        window.location.hash = hash;
      });

    });
  };
  OnePageNavigation();

  var counterInit = function() {
		if ( $('.section-counter').length > 0 ) {
			$('.section-counter').waypoint( function( direction ) {

				if( direction === 'down' && !$(this.element).hasClass('ftco-animated') ) {

					var comma_separator_number_step = $.animateNumber.numberStepFactories.separator(',')
					$('.number').each(function(){
						var $this = $(this),
							num = $this.data('number');
							console.log(num);
						$this.animateNumber(
						  {
						    number: num,
						    numberStep: comma_separator_number_step
						  }, 7000
						);
					});
					
				}

			} , { offset: '95%' } );
		}

	}
	counterInit();
  
});



//password check


var password = document.querySelector('.password');


var validPassword = {
  charLength: document.querySelector('.valid-password .length'),
  lowercase: document.querySelector('.valid-password .lowercase'),
  uppercase: document.querySelector('.valid-password .uppercase'),
  number: document.querySelector('.valid-password .number'),
  special: document.querySelector('.valid-password .special')
};
    
var pattern = {
  
  charLength: function() {
    if( password.value.length >= 8 && password.value.length <= 20 ) {
      return true;
    }
  },
  
  lowercase: function() {
    var regex = /^(?=.*[a-z]).+$/; // Lowercase character pattern
    if( regex.test(password.value) ) {
      return true;
    }
  },
  
  uppercase: function() {
    var regex = /^(?=.*[A-Z]).+$/; // Uppercase character pattern
    if( regex.test(password.value) ) {
      return true;
    }
  },
  
  number: function() {
    var regex = /^(?=.*[0-9]).+$/; // Number check
    if( regex.test(password.value) ) {
      return true;
    }
  },
  
  special: function() {
    var regex = /^(?=.*[_\W]).+$/; // Special character 
    if( regex.test(password.value) ) {
      return true;
    }
  }   
    };
    
// Listen for keyup action on password field
password.addEventListener('keyup', function (){
  patternTest( pattern.charLength(), validPassword.charLength );
  patternTest( pattern.lowercase(), validPassword.lowercase );
  patternTest( pattern.uppercase(), validPassword.uppercase );
  patternTest( pattern.number(), validPassword.number );
  patternTest( pattern.special(), validPassword.special );
    
  // Check that all requirements are fulfilled
  if( hasClass(validPassword.charLength, 'valid') &&
     hasClass(validPassword.lowercase, 'valid') && 
     hasClass(validPassword.uppercase, 'valid') && 
     hasClass(validPassword.number, 'valid') &&
     hasClass(validPassword.special, 'valid')
    ) {
    addClass(password.parentElement, 'valid');
  }
  else {
    removeClass(password.parentElement, 'valid');
  }
});


// Pattern Test function
function patternTest(pattern, response) {
  if(pattern) {
    addClass(response, 'valid');
  }
  else {
    removeClass(response, 'valid');
  }
}

// Has Class Function 
function hasClass(el, className) {
  if (el.classList) {
    return el.classList.contains(className);    
  }
  else {
    new RegExp('(^| )' + className + '( |$)', 'gi').test(el.className); 
  }
}
    
// Add Class Function
function addClass(el, className) {
  if (el.classList) {
    el.classList.add(className);
  }
  else {
    el.className += ' ' + className;
  }
}
  
// Remove Class Function
function removeClass(el, className) {
  if (el.classList)
    el.classList.remove(className);
  else
    el.className = el.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
}